// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2022 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use rocket::response::content;
use rocket_dyn_templates::Template;
use serde::Serialize;
use std::collections::BTreeMap;
use tokio::fs;

type Data = BTreeMap<String, BTreeMap<String, Vec<String>>>;

/// The context needed to render the "index.html" template
#[derive(Serialize)]
struct IndexTemplate {
    users: BTreeMap<String, (usize, String)>,
}

#[derive(Serialize)]
struct DeveloperTemplate {
    name: String,
    email: String,
    url: String,
}

async fn load_data() -> Data {
    serde_json::from_str(
        &fs::read_to_string("data.json")
            .await
            .expect("failed to read data.json"),
    )
    .expect("failed to parse data.json")
}

#[get("/")]
async fn index() -> Template {
    let data = load_data().await;
    let mut users: BTreeMap<_, _> = BTreeMap::new();
    for (author, commits) in data {
        let total: usize =
            commits.into_values().map(|commits| commits.len()).sum();
        // Too lazy to properly URL encode
        let encoded = author.replace('+', "%2B");
        users.insert(author, (total, encoded));
    }
    Template::render("index", IndexTemplate { users })
}

#[get("/data.json")]
async fn data() -> content::RawJson<String> {
    content::RawJson(
        fs::read_to_string("data.json")
            .await
            .expect("unable to read data.json"),
    )
}

#[get("/developer?<name>")]
async fn developer(name: String) -> Template {
    let data = load_data().await;
    let info = data.get(&name).expect("didn't find developer");
    let mut email = vec![include_str!("../email.txt").to_string()];
    for (repository, commits) in info {
        email.push(format!(
            "* {}",
            repository.replace("gerrit-replica", "gerrit")
        ));
        for commit in commits {
            email.push(format!("  * {commit}"));
        }
        email.push("".to_string());
    }
    let email = email.join("\n");
    let url = format!(
        "mailto:board-elections@lists.wikimedia.org?subject=Voting in board election as developer&body={}",
        email.replace('\n', "%0A")
    );
    Template::render("developer", DeveloperTemplate { name, email, url })
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index, developer, data])
        .attach(Template::fairing())
}
